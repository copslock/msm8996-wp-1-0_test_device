//============================================================================
//   Title: std_memorymap
//
//   License: License
//   Copyright 2012-2015 Qualcomm Technologies Inc
//
//   Description: This script setup the memory map for the target
//
//   Input: None
//
//   Output: None
//
//   Usage: do std_memorymap
//
//   Team: CoreBSP Products 8994
//
//   Target: MSM8974
//
//
//   Location: Perforce Revision
//
// when       who           what, where, why
// --------   ---           ---------------------------------------------------------
// 06/15/2015 JBILLING      Fixes for RPM debug             
// 04/14/2015 JBILLING      Updates for IPA                 
// 03/17/2015 JBILLING      MOre updates for Istari, SLPI   
// 12/29/2014 JBILLING      Updated for Istari              
// 07/02/2014 JBILLING      Updated for memorymap changes   
// 05/04/2014 AMCHERIY      Updated for Elessar             
// 02/20/2013 AMCHERIY      Relocatable image support        
// 04/02/2013 AMCHERIY      Updated DDR addresses            
// 01/10/2013 AMCHERIY      Correct RPM addresses            
// 10/19/2012 AMCHERIY      Added more debug cookies        
// 10/10/2012 AMCHERIY      Added debug cookies to map        
// 08/03/2012 AMCHERIY      Corrected addresses                
// 05/03/2012 AMCHERIY      First version for MSM8974        
// 05/03/2012 AMCHERIY      Re-written for B-family            




/////////////////////////////////////////
////////ADSP Memory Map variables////////
/////////////////////////////////////////
GLOBAL &ADSP_EFS_start
GLOBAL &ADSP_EFS_end
GLOBAL &ADSP_EFS_size
GLOBAL &ADSP_EFS_log
&ADSP_EFS_start=0X85F80000
&ADSP_EFS_size=0X20000
&ADSP_EFS_end=&ADSP_EFS_start+&ADSP_EFS_size
&ADSP_EFS_log="ADSP_EFS_log.bin"

// ADSP SW Image - 25 MB
GLOBAL &ADSP_SW_start
GLOBAL &ADSP_SW_end
GLOBAL &ADSP_SW_size
GLOBAL &ADSP_SW_log
&ADSP_SW_start=0x8E000000
&ADSP_SW_size=0x1800000
&ADSP_SW_end=&ADSP_SW_start+&ADSP_SW_size
&ADSP_SW_log="ADSP_SW_log.bin"

GLOBAL &ADSP_TCM_start
GLOBAL &ADSP_VIRT_START_ISLAND
GLOBAL &ADSP_ISLAND_SIZE
&ADSP_TCM_start=0x09400000
&ADSP_VIRT_START_ISLAND=0xF0000000
&ADSP_ISLAND_SIZE=0x80000

/////////////////////////////////////////
////////SLPI Memory Map variables////////
/////////////////////////////////////////

// SLPI SW Image - 25 MB
GLOBAL &SLPI_SW_start
GLOBAL &SLPI_SW_end
GLOBAL &SLPI_SW_size
GLOBAL &SLPI_SW_log
&SLPI_SW_start=0x8E400000
&SLPI_SW_size=0x1900000
&SLPI_SW_end=&SLPI_SW_start+&SLPI_SW_size

&SLPI_SW_log="SLPI_SW_log.bin"

GLOBAL &SLPI_TCM_start
GLOBAL &SLPI_VIRT_START_ISLAND
GLOBAL &SLPI_ISLAND_SIZE
&SLPI_TCM_start=0X01800000
&SLPI_VIRT_START_ISLAND=0xB0000000
&SLPI_ISLAND_SIZE=0x7D000


/////////////////////////////////////////
////////RPM  Memory Map variables////////
/////////////////////////////////////////
// RPM Code RAM - 160 KB
GLOBAL &CODERAM_start
GLOBAL &CODERAM_RPM_start
GLOBAL &CODERAM_end
GLOBAL &CODERAM_size
GLOBAL &CODERAM_log
GLOBAL &CODERAM_USB_log
&CODERAM_start=0X200000
&CODERAM_RPM_start=0X200000
&CODERAM_size=0X24000
&CODERAM_end=&CODERAM_start+&CODERAM_size
&CODERAM_log="CODERAM_log.bin"
&CODERAM_USB_log="CODERAM.bin"

// RPM Data RAM - 80 kB 
GLOBAL &DATARAM_start
GLOBAL &DATARAM_RPM_start
GLOBAL &DATARAM_end
GLOBAL &DATARAM_size
GLOBAL &DATARAM_log
GLOBAL &DATARAM_USB_log
&DATARAM_start=0X290000
&DATARAM_RPM_start=0x290000
&DATARAM_size=0X14000
&DATARAM_end=&DATARAM_start+&DATARAM_size

&DATARAM_log="DATARAM_log.bin"
&DATARAM_USB_log="DATARAM.bin"

// RPM Message RAM - 16KB
GLOBAL &MSGRAM_start
GLOBAL &MSGRAM_end
GLOBAL &MSGRAM_size
GLOBAL &MSGRAM_log
GLOBAL &MSGRAM_USB_log
&MSGRAM_start=0X00068000
&MSGRAM_size=0X6000
&MSGRAM_end=&MSGRAM_start+&MSGRAM_size
&MSGRAM_log="MSGRAM_log.bin"
&MSGRAM_USB_log="MSGRAM.bin"


/////////////////////////////////////////
////////PBL  Memory Map variables////////
/////////////////////////////////////////
GLOBAL &PBL_emmc_init_done
&PBL_emmc_init_done=0x10D220

/////////////////////////////////////////
////////HLOS Memory Map variables////////
/////////////////////////////////////////
GLOBAL &HLOS_1_start
GLOBAL &HLOS_1_end
GLOBAL &HLOS_1_size
GLOBAL &HLOS_1_log
&HLOS_1_start=0
&HLOS_1_end=0X83FFFFF
&HLOS_1_size=0X8400000
&HLOS_1_log="HLOS_1_log.bin"


GLOBAL &HLOS_2_start
GLOBAL &HLOS_2_end
GLOBAL &HLOS_2_size
GLOBAL &HLOS_2_log
&HLOS_2_start=0XFF00000
&HLOS_2_end=0XFFFFFFF
&HLOS_2_size=0X100000
&HLOS_2_log="HLOS_2_log.bin"


GLOBAL &HLOS_3_start
GLOBAL &HLOS_3_end
GLOBAL &HLOS_3_size
GLOBAL &HLOS_3_log
&HLOS_3_start=0X11000000
&HLOS_3_end=0X1FFFFFFF
&HLOS_3_size=0XEFFFFFF
&HLOS_3_log="HLOS_3_log.bin"


GLOBAL &HLOS_4_start
GLOBAL &HLOS_4_end
GLOBAL &HLOS_4_size
GLOBAL &HLOS_4_log
&HLOS_4_start=0X20000000
&HLOS_4_end=0X7FFFFFFF
&HLOS_4_size=0X5FFFFFFF
&HLOS_4_log="HLOS_4_log.bin"


/////////////////////////////////////////
////////MBA  Memory Map variables////////
/////////////////////////////////////////
GLOBAL &MBA_Meta_start
GLOBAL &MBA_Meta_end
GLOBAL &MBA_Meta_size
GLOBAL &MBA_Meta_log
&MBA_Meta_start=0XD1FC000
&MBA_Meta_end=0XD1FFFFF
&MBA_Meta_size=0X4000
&MBA_Meta_log="MBA_Meta_log.bin"


GLOBAL &MBA_SW_start
GLOBAL &MBA_SW_end
GLOBAL &MBA_SW_size
GLOBAL &MBA_SW_log
&MBA_SW_start=0XD100000
&MBA_SW_size=0XFC000
&MBA_SW_end=&MBA_SW_start+&MBA_SW_size

&MBA_SW_log="MBA_SW_log.bin"

/////////////////////////////////////////
////////MPSS Memory Map variables////////
/////////////////////////////////////////
GLOBAL &MPSS_EFS_start
GLOBAL &MPSS_EFS_end
GLOBAL &MPSS_EFS_size
GLOBAL &MPSS_EFS_log
&MPSS_EFS_start=0X85E00000
&MPSS_EFS_size=0X180000
&MPSS_EFS_end=&MPSS_EFS_start+&MPSS_EFS_size
&MPSS_EFS_log="MPSS_EFS_log.bin"


// MPSS Image - 77 MB
GLOBAL &MPSS_SW_start
GLOBAL &MPSS_SW_end
GLOBAL &MPSS_SW_size
GLOBAL &MPSS_SW_log
&MPSS_SW_start=0x88800000
&MPSS_SW_size=0x5800000
&MPSS_SW_end=&MPSS_SW_start+&MPSS_SW_size
&MPSS_SW_log="MPSS_SW_log.bin"

GLOBAL &MPSS_TCM_start
&MPSS_TCM_start=0x02400000


/////////////////////////////////////////
////////QDSS Memory Map variables////////
/////////////////////////////////////////
// QDSS Image - 16 MB
GLOBAL &QDSS_SW_start
GLOBAL &QDSS_SW_end
GLOBAL &QDSS_SW_size
GLOBAL &QDSS_SW_log
&QDSS_SW_start=0X10000000
&QDSS_SW_end=0X10FFFFFF
&QDSS_SW_size=0x1000000
&QDSS_SW_log="QDSS_SW_log.bin"


/////////////////////////////////////////
////////TZ   Memory Map variables////////
/////////////////////////////////////////
GLOBAL &TZ_SW_start
GLOBAL &TZ_SW_end
GLOBAL &TZ_SW_size
GLOBAL &TZ_SW_log
&TZ_SW_start=0X86200000
&TZ_SW_size=0X100000
&TZ_SW_end=&TZ_SW_start+&TZ_SW_size
&TZ_SW_log="TZ_SW_log.bin"


GLOBAL &PIMEM_start
GLOBAL &PIMEM_end
GLOBAL &PIMEM_size
GLOBAL &PIMEM_log
GLOBAL &PIMEM_USB_log
&PIMEM_start=0XB000000
&PIMEM_size=0X200000
&PIMEM_end=&PIMEM_start+&PIMEM_size
&PIMEM_log="PIMEM_log.bin"
&PIMEM_USB_log="PIMEM.bin"
/////////////////////////////////////////
////////VSS  Memory Map variables////////
/////////////////////////////////////////
// VSS Image - 5 MB
GLOBAL &VSS_SW_start
GLOBAL &VSS_SW_end
GLOBAL &VSS_SW_size
GLOBAL &VSS_SW_log
&VSS_SW_start=0X90200000
&VSS_SW_size=0X500000
&VSS_SW_end=&VSS_SW_start+&VSS_SW_size

&VSS_SW_log="Venus_SW_log.bin"

/////////////////////////////////////////
////////IPA  Memory Map variables////////
/////////////////////////////////////////
//0x7950000++0x4000          - IPA_IRAM.bin   --> Load script should load this region at address 0x0 into the CM3 simulator
//0x7954000--0x7957eff      - IPA_DRAM.bin  --> Load script should load this region at address 0x4000 into the CM3 simulator
//0x7945000--0x7946FFF     - IPA_SRAM.bin   (IPA SRAM: SW-Area)
//0x7949000--0x794BEFF     - IPA_HRAM.bin  (IPA SRAM: HW-Area)
//0x794F000-0x794FFFF      - IPA_DICT.bin (Decompression Dictionary)
//0x7962000--0x79620FF    - IPA_MBOX.bin
//0x7904000++0x26fff        - IPA_REG1.bin
//0x7940000++0x4fff          - IPA_REG2.bin
//0x7960000++0x1fff          - IPA_REG3.bin

////////IPA_IRAM.bin///////
GLOBAL &IPA_IRAM_log
GLOBAL &IPA_IRAM_IPA_start
GLOBAL &IPA_IRAM_start
GLOBAL &IPA_IRAM_end
GLOBAL &IPA_IRAM_size
&IPA_IRAM_log="IPA_IRAM.bin"
&IPA_IRAM_IPA_start=0x0
&IPA_IRAM_start=0x7950000
&IPA_IRAM_size=0x4000
&IPA_IRAM_end=&IPA_IRAM_start+&IPA_IRAM_size //0x7953FFF

////////IPA_DRAM.bin///////
GLOBAL &IPA_DRAM_log
GLOBAL &IPA_DRAM_IPA_start
GLOBAL &IPA_DRAM_start
GLOBAL &IPA_DRAM_end
GLOBAL &IPA_DRAM_size
&IPA_DRAM_log="IPA_DRAM.bin"
&IPA_DRAM_IPA_start=0x4000
&IPA_DRAM_start=0x7954000
&IPA_DRAM_size=0x3F00
&IPA_DRAM_end=&IPA_DRAM_start+&IPA_DRAM_size //0x7957eff

////////IPA_SRAM.bin///////
GLOBAL &IPA_SRAM_log 
GLOBAL &IPA_SRAM_start
GLOBAL &IPA_SRAM_end
GLOBAL &IPA_SRAM_size
&IPA_SRAM_log="IPA_SRAM.bin" 
&IPA_SRAM_start=0x7945000
&IPA_SRAM_size=0x1FFF
&IPA_SRAM_end=&IPA_SRAM_start+&IPA_SRAM_size  //0x7946FFF

////////IPA_HRAM.bin///////
GLOBAL &IPA_HRAM_log 
GLOBAL &IPA_HRAM_start
GLOBAL &IPA_HRAM_end
GLOBAL &IPA_HRAM_size
&IPA_HRAM_log="IPA_HRAM.bin" 
&IPA_HRAM_start=0x7949000
&IPA_HRAM_size=0x2EFF
&IPA_HRAM_end=&IPA_HRAM_start+&IPA_HRAM_size  //0x794BEFF

////////IPA_DICT.bin///////
GLOBAL &IPA_DICT_log 
GLOBAL &IPA_DICT_start
GLOBAL &IPA_DICT_end
GLOBAL &IPA_DICT_size
&IPA_DICT_log="IPA_DICT.bin" 
&IPA_DICT_start=0x794F000
&IPA_DICT_size=0xFFF
&IPA_DICT_end=&IPA_DICT_start+&IPA_DICT_size  //0x794FFFF

////////IPA_MBOX.bin///////
GLOBAL &IPA_MBOX_log 
GLOBAL &IPA_MBOX_start
GLOBAL &IPA_MBOX_end
GLOBAL &IPA_MBOX_size
&IPA_MBOX_log="IPA_MBOX.bin" 
&IPA_MBOX_start=0x7962000
&IPA_MBOX_size=0xFF
&IPA_MBOX_end=&IPA_MBOX_start+&IPA_MBOX_size  //0x79620FF

////////IPA_REG1.bin///////
GLOBAL &IPA_REG1_log
GLOBAL &IPA_REG1_start
GLOBAL &IPA_REG1_end
GLOBAL &IPA_REG1_size
&IPA_REG1_log="IPA_REG1.bin"
&IPA_REG1_start=0x7904000
&IPA_REG1_size=0x26fff
&IPA_REG1_end=&IPA_REG1_start+&IPA_REG1_size  //0x792AFFF

////////IPA_REG2.bin///////
GLOBAL &IPA_REG1_log
GLOBAL &IPA_REG1_start
GLOBAL &IPA_REG1_end
GLOBAL &IPA_REG1_size
&IPA_REG1_log="IPA_REG2.bin"
&IPA_REG1_start=0x7940000
&IPA_REG1_size=0x4fff
&IPA_REG1_end=&IPA_REG1_start+&IPA_REG1_size  //0x7944FFF

////////IPA_REG3.bin///////
GLOBAL &IPA_REG1_log
GLOBAL &IPA_REG1_start
GLOBAL &IPA_REG1_end
GLOBAL &IPA_REG1_size
&IPA_REG1_log="IPA_REG3.bin"
&IPA_REG1_start=0x7960000
&IPA_REG1_size=0x1fff
&IPA_REG1_end=&IPA_REG1_start+&IPA_REG1_size  //0x7961fff

GLOBAL &IPA_regs
GLOBAL &IPA_mmu
&IPA_regs="IPA_regs.cmm"
&RPM_mmu="IPA_regs.cmm"


/////////////////////////////////////////
////////DDR  Memory Map variables////////
/////////////////////////////////////////
// DDR memory: combined space used by all images
GLOBAL &DDR_1_start
GLOBAL &DDR_1_end
GLOBAL &DDR_1_size
GLOBAL &DDR_1_log
GLOBAL &DDR_1_USB_log
&DDR_1_start=0x80000000
&DDR_1_size=0x1FFFFFFF
&DDR_1_end=&DDR_1_start+&DDR_1_size
&DDR_1_log="DDR0CS0.BIN"
&DDR_1_USB_log="DDRCS0.BIN"

GLOBAL &DDR_2_start
GLOBAL &DDR_2_end
GLOBAL &DDR_2_size
GLOBAL &DDR_2_log
GLOBAL &DDR_2_USB_log
&DDR_2_start=0xA0000000
&DDR_2_size=0x1FFFFFFF
&DDR_2_end=&DDR_2_start+&DDR_2_size
&DDR_2_log="DDR1CS0.BIN"
&DDR_2_USB_log="DDRCS1.BIN"

GLOBAL &DDR_3_start
GLOBAL &DDR_3_end
GLOBAL &DDR_3_size
GLOBAL &DDR_3_log
GLOBAL &DDR_3_USB_log
&DDR_3_start=0xC0000000
&DDR_3_size=0x1FFFFFFF
&DDR_3_end=&DDR_3_start+&DDR_3_size

&DDR_3_log="DDR0CS1.BIN"
&DDR_3_USB_log="DDRCS3.BIN"

GLOBAL &DDR_4_start
GLOBAL &DDR_4_end
GLOBAL &DDR_4_size
GLOBAL &DDR_4_log
GLOBAL &DDR_4_USB_log
&DDR_4_start=0xE0000000
&DDR_4_size=0x1FFFFFFF
&DDR_4_end=&DDR_4_start+&DDR_4_size

&DDR_4_log="DDR1CS1.BIN"
&DDR_4_USB_log="DDRCS4.BIN"



// DDR memory sub-section - combined space used by all peripherals
// Saving this region will give all peripherals in one go
GLOBAL &DDR_Periph_start
GLOBAL &DDR_Periph_end
GLOBAL &DDR_Periph_size
GLOBAL &DDR_Periph_log
&DDR_Periph_start=0x6300000
&DDR_Periph_end=0x10000000
&DDR_Periph_size=0x9CFFFFF
&DDR_Periph_log="DDRPeriph.bin"


/////////////////////////////////////////
////////SMEM Memory Map variables////////
/////////////////////////////////////////
GLOBAL &SMEM_start
GLOBAL &SMEM_end
GLOBAL &SMEM_size
GLOBAL &SMEM_log
&SMEM_start=0XFA00000
&SMEM_end=0XFBFFFFF
&SMEM_size=0X200000
&SMEM_log="SMEM_log.bin"



/////////////////////////////////////////
/////SharedIMEM Memory Map variables/////
/////////////////////////////////////////
GLOBAL &SHARED_IMEM_start
GLOBAL &SHARED_IMEM_end
GLOBAL &SHARED_IMEM_size
GLOBAL &SHARED_IMEM_log
GLOBAL &SHARED_IMEM_USB_log
&SHARED_IMEM_start=0x066BF000
&SHARED_IMEM_size=0x40000
&SHARED_IMEM_end=&SHARED_IMEM_start+&SHARED_IMEM_size

//Shared IMEM is part of OCIMEM
&SHARED_IMEM_log="OCIMEM.bin"







/////////////////////////////////////////////
//////// Relocatable Image support///////////
/////////////////////////////////////////////
GLOBAL &RELOCATION_DATA_start
GLOBAL &RELOCATION_DATA_end
GLOBAL &RELOCATION_DATA_size
&RELOCATION_DATA_start=&SHARED_IMEM_start+0x94C
&RELOCATION_DATA_size=0xC8
&RELOCATION_DATA_end=&RELOCATION_DATA_start+&RELOCATION_DATA_size-0x1

// Each entry in the table is in the following format
// 8 bytes - image name
// 8 bytes - image start address
// 4 bytes - image size


//////////////////////////////////////////////////////
// This region is in the Shared IMEM block          //
// These are the cookies used to debug any image    //
// Allocation varies from target to target          //
//////////////////////////////////////////////////////
GLOBAL &DEBUG_REGION_START
GLOBAL &DEBUG_REGION_END
GLOBAL &DEBUG_REGION_SIZE
GLOBAL &DEBUG_COOKIE_VALUE
GLOBAL &XBL_DEBUG_COOKIE
GLOBAL &MBA_DEBUG_COOKIE
GLOBAL &MPSS_DEBUG_COOKIE
GLOBAL &ADSP_DEBUG_COOKIE
GLOBAL &SLPI_DEBUG_COOKIE
GLOBAL &WCNSS_DEBUG_COOKIE
GLOBAL &APPSBOOT_DEBUG_COOKIE
GLOBAL &MDM_DEBUG_COOKIE
GLOBAL &RPM_DEBUG_COOKIE
&DEBUG_COOKIE_VALUE=0x53444247
//&DEBUG_REGION_START=0x66BF934
&DEBUG_REGION_START=&SHARED_IMEM_start+0x934
//&DEBUG_REGION_END=0x66BF94C
&DEBUG_REGION_SIZE=0x18
&DEBUG_REGION_END=&DEBUG_REGION_START+&DEBUG_REGION_SIZE

&XBL_DEBUG_COOKIE=&DEBUG_REGION_START+0x10 //0x66BF944
&MBA_DEBUG_COOKIE=&DEBUG_REGION_START+0x4 //0x66BF938
&MPSS_DEBUG_COOKIE=&DEBUG_REGION_START+0x0 //0x66BF934
&ADSP_DEBUG_COOKIE=&DEBUG_REGION_START+0x8 //0x66BF93C
&SLPI_DEBUG_COOKIE=&DEBUG_REGION_START+0x8 //0x66BF93C
&WCNSS_DEBUG_COOKIE=&DEBUG_REGION_START+0xC //0x66BF940
&APPSBOOT_DEBUG_COOKIE=&DEBUG_REGION_START+0x14 //0x66BF948
&MDM_DEBUG_COOKIE="&MPSS_DEBUG_COOKIE"
&RPM_DEBUG_COOKIE=&SHARED_IMEM_start+0xB18 //0x66bfb18


/////////////////////////////////////////////
//////// OCIMEM Memory Region ///////////////
/////////////////////////////////////////////
GLOBAL &OCIMEM_start
GLOBAL &OCIMEM_end
GLOBAL &OCIMEM_size
GLOBAL &OCIMEM_log
GLOBAL &OCIMEM_USB_log
&OCIMEM_start=0x6680000
&OCIMEM_size=0x80000
&OCIMEM_end=&OCIMEM_start+&OCIMEM_size
&OCIMEM_log="OCIMEM.bin"
&OCIMEM_USB_log="OCIMEM.bin"

/////////////////////////////////////////////
//////// Various Registers///////////////////
/////////////////////////////////////////////
GLOBAL &WCNSS_regs
GLOBAL &WCNSS_mmu
&WCNSS_regs="WCNSS_regs.cmm"
&WCNSS_mmu="WCNSS_mmu.cmm"

GLOBAL &RPM_regs
GLOBAL &RPM_mmu
&RPM_regs="RPM_regs.cmm"
&RPM_mmu="RPM_regs.cmm"

GLOBAL &MPSS_Thread0_regs
&MPSS_Thread0_regs="MPSS_Thread0_regs.cmm"
GLOBAL &MPSS_Thread1_regs
&MPSS_Thread1_regs="MPSS_Thread1_regs.cmm"
GLOBAL &MPSS_Thread2_regs
&MPSS_Thread2_regs="MPSS_Thread2_regs.cmm"
GLOBAL &MPSS_Thread3_regs
&MPSS_Thread3_regs="MPSS_Thread3_regs.cmm"

GLOBAL &ADSP_Thread0_regs
&ADSP_Thread0_regs="ADSP_Thread0_regs.cmm"
GLOBAL &ADSP_Thread1_regs
&ADSP_Thread1_regs="ADSP_Thread1_regs.cmm"
GLOBAL &ADSP_Thread2_regs
&ADSP_Thread2_regs="ADSP_Thread2_regs.cmm"
GLOBAL &ADSP_Thread3_regs
&ADSP_Thread3_regs="ADSP_Thread3_regs.cmm"

GLOBAL &SLPI_Thread0_regs
&SLPI_Thread0_regs="SLPI_Thread0_regs.cmm"
GLOBAL &SLPI_Thread1_regs
&SLPI_Thread1_regs="SLPI_Thread1_regs.cmm"
GLOBAL &SLPI_Thread2_regs
&SLPI_Thread2_regs="SLPI_Thread2_regs.cmm"
GLOBAL &SLPI_Thread3_regs
&SLPI_Thread3_regs="SLPI_Thread3_regs.cmm"


GLOBAL &KPSS_Core0_regs
GLOBAL &KPSS_Core0_mmu
&KPSS_Core0_regs="KPSS_Core0_regs.cmm"
&KPSS_Core0_mmu="KPSS_Core0_mmu.cmm"

GLOBAL &KPSS_Core1_regs
GLOBAL &KPSS_Core1_mmu
&KPSS_Core1_regs="KPSS_Core1_regs.cmm"
&KPSS_Core1_mmu="KPSS_Core1_mmu.cmm"

GLOBAL &KPSS_Core2_regs
GLOBAL &KPSS_Core2_mmu
&KPSS_Core2_regs="KPSS_Core2_regs.cmm"
&KPSS_Core2_mmu="KPSS_Core2_mmu.cmm"

GLOBAL &KPSS_Core3_regs
GLOBAL &KPSS_Core3_mmu
&KPSS_Core3_regs="KPSS_Core3_regs.cmm"
&KPSS_Core3_mmu="KPSS_Core3_mmu.cmm"


/////////////////////////////////////////////
/////////////////////////////////////////////
/////////END Global Constants Section////////
/////////////////////////////////////////////
/////////////////////////////////////////////



MAIN:
    ENTRY &LoadOption
   LOCAL &result
    // Check if any attributes obtained at run time are needed
    If ("&LoadOption"=="DYNAMIC")
    (
            // Change any defaults
            IF (!SIMULATOR())
            (
                // Get relocated image info again
                GOSUB PARSE_IMAGE_RELOCATION
                ENTRY %LINE &result
            )
            ELSE
            (
                // Do both together on simulator
                GOSUB RELOC_SIMEM_N_PARSE_IMAGE_RELOC
                ENTRY %LINE &result
            )
    )

    GOSUB EXIT &result

EXIT:
    LOCAL &rvalue
    ENTRY %LINE &rvalue
    ENDDO &rvalue



// Function to parse relocated image data that is 
// saved in internal memory
// This function will be run once only unless a new 
// session has started
PARSE_IMAGE_RELOCATION:
    LOCAL &HEAD &TAIL &IMG_NAME &IMG_START &IMG_SIZE

    &HEAD=&RELOCATION_DATA_start
    &TAIL=&RELOCATION_DATA_end

    ON ERROR GOSUB
    (
        PRINT %ERROR "Error ocurred while reading relocation data from device"
        RETURN "Error ocurred while reading relocation data from device"
    )
    WHILE (&HEAD<=&TAIL)
    (
        // Null terminate the string
        DATA.SET EA:&HEAD+0x4 %LONG DATA.LONG(EA:&HEAD+0x4)&0xFFFFFF
         &IMG_NAME=DATA.STRING(EA:&HEAD)
        &IMG_START=DATA.LONG(EA:&HEAD+0xC)<<32.|DATA.LONG(EA:&HEAD+0x8)
        &IMG_SIZE=DATA.LONG(EA:&HEAD+0x10)

        // We have 5 images supported. No error if an image is not present
        IF ("&IMG_NAME"=="slpi")
        (
            &SLPI_SW_start=&IMG_START
            &SLPI_SW_size=&IMG_SIZE
            &SLPI_SW_end=&IMG_START+&IMG_SIZE-1
        )
        
        // We have 5 images supported. No error if an image is not present
        IF ("&IMG_NAME"=="adsp")
        (
            &ADSP_SW_start=&IMG_START
            &ADSP_SW_size=&IMG_SIZE
            &ADSP_SW_end=&IMG_START+&IMG_SIZE-1
        )

        IF ("&IMG_NAME"=="modem")
        (
            &MPSS_SW_start=&IMG_START
            &MPSS_SW_size=&IMG_SIZE
            &MPSS_SW_end=&IMG_START+&IMG_SIZE-1
        )

        IF ("&IMG_NAME"=="wcnss")
        (
            &WCNSS_SW_start=&IMG_START
            &WCNSS_SW_size=&IMG_SIZE
            &WCNSS_SW_end=&IMG_START+&IMG_SIZE-1
        )

        // Move to the next entry
        &HEAD=&HEAD+0x14
    )

    RETURN SUCCESS


// Function to parse relocated image data that is 
// saved in internal memory and identify shared IMEM location
// This function will be run once only unless a new debug
// session has started    
RELOC_SIMEM_N_PARSE_IMAGE_RELOC:
    LOCAL &HEAD &TAIL &IMG_NAME &IMG_START &IMG_SIZE 
    LOCAL &RELOCATED &COUNT

    // Init locals
    &RELOCATED=0
    &COUNT=0

    // Start with default location for shared IMEM
    WHILE ((&RELOCATED==0)&&(&COUNT<1))
    (
        &HEAD=&RELOCATION_DATA_start
        &TAIL=&RELOCATION_DATA_end
        WHILE (&HEAD<=&TAIL)
        (
            // Null terminate the string
            DATA.SET EA:&HEAD+0x4 %LONG DATA.LONG(EA:&HEAD+0x4)&0xFFFFFF
             &IMG_NAME=DATA.STRING(EA:&HEAD)
            &IMG_START=DATA.LONG(EA:&HEAD+0xC)<<32.|DATA.LONG(EA:&HEAD+0x8)
            &IMG_SIZE=DATA.LONG(EA:&HEAD+0x10)

            // We have 5 images supported. No error if an image is not present
            IF (STR.LWR("&IMG_NAME")=="slpi")
            (
                &SLPI_SW_start=&IMG_START
                &SLPI_SW_size=&IMG_SIZE
                &SLPI_SW_end=&IMG_START+&IMG_SIZE-1
                &RELOCATED=1
            )
            
            IF (STR.LWR("&IMG_NAME")=="adsp")
            (
                &ADSP_SW_start=&IMG_START
                &ADSP_SW_size=&IMG_SIZE
                &ADSP_SW_end=&IMG_START+&IMG_SIZE-1
                &RELOCATED=1
            )

            IF (STR.LWR("&IMG_NAME")=="modem")
            (
                &MPSS_SW_start=&IMG_START
                &MPSS_SW_size=&IMG_SIZE
                &MPSS_SW_end=&IMG_START+&IMG_SIZE-1
                &RELOCATED=1
            )

            IF (STR.LWR("&IMG_NAME")=="wcnss")
            (
                &WCNSS_SW_start=&IMG_START
                &WCNSS_SW_size=&IMG_SIZE
                &WCNSS_SW_end=&IMG_START+&IMG_SIZE-1
                &RELOCATED=1
            )

            // Move to the next entry
            &HEAD=&HEAD+0x14
        )

        // Increment count
        &COUNT=&COUNT+1

    )

    // If we reach this point without finding relocated data, it means 911 time !
    IF (&RELOCATED==0)
    (
        PRINT "Relocated data not found at : &RELOCATION_DATA_start"
        RETURN "FAILURE - Relocated data not found at : &RELOCATION_DATA_start"
    )
    ELSE
    (
        PRINT "Relocated data found at : &RELOCATION_DATA_start"
        RETURN SUCCESS
    )
    //Should never get here
    RETURN


    

